const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = async (req, res) => {
    const result = await Bicicleta.allBicis();
    res.status(200).json({
        bicicletas: result
    });
}

exports.bicicleta_create = async (req, res) => {
    const bici = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    await Bicicleta.add(bici);

    res.status(201).json({
        bicicleta: bici
    });
}

exports.bicicleta_remove = async (req, res) => {
    await Bicicleta.removeByCode(req.body.id);

    res.status(204).send();
}

exports.bicicleta_update = async (req, res) => {
    const bici = await Bicicleta.findByCode(req.body.id);
    bici.modelo = req.body.modelo;
    bici.color = req.body.color;
    bici.ubicacion = [req.body.lat, req.body.lng];
    await Bicicleta.add(bici);

    res.status(200).send();
}