const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

const apiUrl = 'http://localhost:3000/api/bicicletas';

describe('bicicleta API', () => {

    beforeAll(async ()=>{
        await mongoose.disconnect();
    });

    beforeEach((done) => {

        const mongodb = 'mongodb://localhost/testdb'
        mongoose.connect(mongodb, { 
            useNewUrlParser: true, 
            useUnifiedTopology: true, 
            useCreateIndex: true
        });
        let db = mongoose.connection;
        db.on('error', console.error.bind(console, 'conecction error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done) => {

        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        })
    });
    
    describe('GET bicicletas /', () => {
        it('status 200', (done) => {

            request.get( apiUrl, function(err, res, body ){
                const result = JSON.parse(body);
                expect(res.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST bicicletas/create', () => {
        it('Deberia agregar una bicicleta', (done) => {
            const headers = { 'content-type': 'application/json'};
            const bici = `{"id":1, "color":"verde", "modelo":"nueva", "lat":6.230833, "lng":75.590553}`;
            request.post({
                headers: headers,
                url: apiUrl + '/create',
                body: bici
            }, function(err, res, body ) {
                expect(res.statusCode).toBe(201);
                const bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe('verde');
                expect(bici.ubicacion[0]).toBe(6.230833);
                expect(bici.ubicacion[1]).toBe(75.590553);
                done();
            });
        });
    });

    describe('DELETE bicicletas/remove', () => {
        it('Deberia remover una bicicleta', (done) => {

            const headers = { 'content-type': 'application/json'};
            const bici = `{"id":1, "color":"verde", "modelo":"nueva", "lat":6.230833, "lng":75.590553}`;
            request.post({
                headers: headers,
                url: apiUrl + '/create',
                body: bici
            }, function(err, res, body ) {
                expect(res.statusCode).toBe(201);
                const bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe('verde');
                expect(bici.ubicacion[0]).toBe(6.230833);
                expect(bici.ubicacion[1]).toBe(75.590553);

                request.delete({
                    headers: headers,
                    url: apiUrl + '/remove',
                    body: `{"id":1}`
                }, function(err, res){
                    expect(res.statusCode).toBe(204);
                    
                    request.get( apiUrl, function(err, res, body ){
                        const result = JSON.parse(body);
                        expect(res.statusCode).toBe(200);
                        expect(result.bicicletas.length).toBe(0);
                        done();
                    });
                })
            });
        });
    });

    describe('PUT bicicletas/update', () => {
        it('Deberia actualizar una bicicleta', (done) => {
            const headers = { 'content-type': 'application/json'};
            const bici = `{"id":1, "color":"verde", "modelo":"nueva", "lat":6.230833, "lng":75.590553}`;
            const biciUpdated =  `{"id":1, "color":"rojo", "modelo":"especial", "lat":6.230833, "lng":75.590553}`;
            request.post({
                headers: headers,
                url: apiUrl + '/create',
                body: bici
            }, function(err, res, body ) {
                expect(res.statusCode).toBe(201);
                const bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe('verde');
                expect(bici.ubicacion[0]).toBe(6.230833);
                expect(bici.ubicacion[1]).toBe(75.590553);

                request.put({
                    headers: headers,
                    url: apiUrl + '/update',
                    body: biciUpdated
                }, function(err, res){
                    expect(res.statusCode).toBe(200);

                    request.get( apiUrl, function(err, res, body ){
                        const result = JSON.parse(body).bicicletas;
                        expect(res.statusCode).toBe(200);
                        expect(result[0].color).toBe('rojo');
                        expect(result[0].modelo).toBe('especial');
                        done();
                    });
                })
            });
        });
    });

});