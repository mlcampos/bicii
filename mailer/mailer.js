const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
const env = process.env.NODE_ENV;
let options;
switch( env ) {
    case 'production': {
        options = {
            auth: {
                api_key: process.env.SENDGRID_API_KEY
            }
        }
        mailConfig = sgTransport(options);
        break;
    }
    case 'staging': {
        console.log('XXXXXXXXXXXXXXXXXXXXXXXXX');
        options = {
            auth: {
                api_key: process.env.SENDGRID_API_KEY
            }
        }
        mailConfig = sgTransport(options);
        break;
    }
    default: {
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ETHEREAL_USER,
                pass: process.env.ETHEREAL_PASS
            }
        }
    }
}

const email = nodemailer.createTransport(mailConfig);

exports.sendEmail = (config, cb) => {
    return email.sendMail(config, cb);
}