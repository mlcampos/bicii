const mongoose = require('mongoose');
const Usuario = require('../../models/usuario');
const Bicicleta = require('../../models/bicicleta');
const Reserva = require('../../models/reserva');
const request = require('request');
const server = require('../../bin/www');

const apiUrl = 'http://localhost:3000/api/usuarios';

describe('usuario API', () => {
    beforeAll(async ()=>{
        await mongoose.disconnect();
    });

    beforeEach((done) => {

        const mongodb = 'mongodb://localhost/testdb'
        mongoose.connect(mongodb, { 
            useNewUrlParser: true, 
            useUnifiedTopology: true, 
            useCreateIndex: true
        });
        let db = mongoose.connection;
        db.on('error', console.error.bind(console, 'conecction error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done) => {

        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success) {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });
    
    describe('GET usuarios /', () => {
        it('status 200', (done) => {

            request.get( apiUrl, function(err, res, body ){
                const result = JSON.parse(body);
                expect(res.statusCode).toBe(200);
                expect(result.usuarios.length).toBe(0);
                done();
            });
        });
    });

});