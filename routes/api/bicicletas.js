const express = require('express');
const router = express.Router();
const bicicletaController = require('../../controllers/api/bicicletasControllerApi'); 

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/remove', bicicletaController.bicicleta_remove);
router.put('/update', bicicletaController.bicicleta_update)

module.exports = router;
