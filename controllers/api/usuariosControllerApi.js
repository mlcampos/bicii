const Usuario = require('../../models/usuario');

exports.usuarios_list = (req, res) => {
    Usuario.find({}, function(err, usuarios) {
        res.status(200).json({
            usuarios
        });
    });
};

exports.usuarios_create = (req, res) => {
    const usuario = new Usuario(req.body);
    console.log(usuario);
    usuario.save(function(err) {
        res.status(201).json({usuario});
    });
};

exports.usuarios_reservar = (req, res) => {
    const { id, bici_id, desde, hasta } = req.body;
    Usuario.findById(id, function(err, usuario) {
        usuario.reservar(bici_id, desde, hasta, function(err, reserva) {
            res.status(200).send();
        })
    })
};