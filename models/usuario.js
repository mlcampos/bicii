const mongoose = require('mongoose');
const Reserva = require('./reserva');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const saltRounds = 10;
const crypto = require('crypto');

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const validateEmail = function(email) {
    const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return re.test(email);
}

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        require: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next) {
    if ( this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
    const reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde, hasta});
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;
    token.save(function(err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta✔', 
            text: `Hola, \n\n Por favor, para verificar tu cuenta haga click en este link \n http://localhost:3000/token/confirmation/${token.token}\n`
        };

        // send mail with defined transport object
        mailer.sendEmail(mailOptions, function(error){
            if(error){
                return console.log(error.message);
            }
            console.log('A verification Email has been sent to : ' + email_destination);
        });
    });
}

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;
    token.save(function(err) {
        if (err) {
            console.log(err.message);
            return cb(err);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta✔', 
            text: `Hola, \n\n Por favor, para resetear el password de tu cuenta haga click en este link \n http://localhost:3000/resetPassword/${token.token}`
        };

        // send mail with defined transport object
        mailer.sendEmail(mailOptions, function(error){
            if(error){
                return console.log(error.message);
            }
            console.log('se envio un Email para resetear el password a : ' + email_destination);
        });
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);