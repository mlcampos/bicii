require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var passport = require('./config/passport');
var jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');

var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter = require('./routes/api/usuarios');
var authApiRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token = require('./models/token');

const store = new session.MemoryStore;

var app = express();

app.set('secretkey', 'jwo_pwd_!!223344');
app.set('password_mongodb', 'kXzUP2w8IJJawtum');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis[****]123123'
}));

const mongoose = require('mongoose');
const usuario = require('./models/usuario');
const { error } = require('console');

var mongodb = process.env.MONGO_URI;
mongoose.connect(mongodb, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB conecction error'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req, res) {
  res.render('session/login');
});

app.post('/login', function(req, res, next) {
   passport.authenticate('local', function(err, usuario, info) {
      if (err ) return next(err);
      if (!usuario) return res.render('session/login', { info });
      req.login(usuario, function(err) {
        if (err) return next(err);
        return res.redirect('/');
      });
   })(req, res, next);
});

app.get('/logout', function(req, res) {
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res) {
  res.render('session/forgotPassword')
});

app.post('/forgotPassword', function(req, res, next) {
    Usuario.findOne({email: req.body.email}, function(err, usuario) {
      if (!usuario) return res.render('session/forgotPassword', { info: {message: 'No existe el email para un usuario existente'}});
      usuario.resetPassword(function(err){
        if (err) return next(err);
        console.log('session/forgotPasswordMessage');
      });
      res.render('session/forgotPasswordMessage');
    })
});

app.get('/resetPassword/:token', function(req,res,next) {
  Token.findOne({token: req.params.token}, function(err, token) {
    if (!token) return res.status(400).send({type: 'not-verified', msg: 'No existe un usuario asociado al token. verifique que su token no haya expirado'});

    Usuario.findById(token._userId, function(err, usuario) {
      if (!usuario) return res.status(400).send({msg: 'No existe un usuario asociado al token'});
      res.render('session/resetPassword', { errors: {}, usuario});
    })
  })
});

app.post('/resetPassword', function(req,res,next) {
  const { password, confirm_password, email } = req.body;
  if ( password != confirm_password ) {
    return res.render('session/resetPassword', { errors: { confirm_password: {message: 'No coincide con el password ingresado'}}, usuario: new Usuario({email})});
  }

  Usuario.findOne({email}, function(err, usuario) {
    usuario.password = password;
    usuario.save(function(err) {
      if (err) {
        res.render('session/resetPassword', { errors: error.errors, usuario: new Usuario({email})});
      } else {
        res.redirect('/login');
      }
    });
  })
});



app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas',loggedIn, bicicletasRouter);

app.use('/api/bicicletas', bicicletasApiRouter);
app.use('/api/usuarios',validarUsuario, usuariosApiRouter);
app.use('/api/auth', authApiRouter);

app.use('/privacy_policy', function(req, res) {
  res.sendFile('public/privacy_policy.html')
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log('user sin logear');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next) {
   jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function(err, decoded){
     if (err) {
       res.json({status:'error',message: err.message, data:null})
     } else {
       req.body.userId = decoded.id;
       next();
     }
   });
}

module.exports = app;
