const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bicicletaSchema = Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true} 
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code,
        color,
        modelo,
        ubicacion
    })
}

bicicletaSchema.methods.toString = function() {
    return `Id: ${this.id} | Color: ${this.color} | Modelo: ${this.modelo}`;
}

bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
}

bicicletaSchema.statics.add = function(bicicleta, cb) {
    return this.create(bicicleta, cb);
}

bicicletaSchema.statics.findByCode = function(code, cb) {
    return this.findOne({code:code}, cb);
}

bicicletaSchema.statics.removeByCode = function(code, cb) {
    return this.deleteOne({code:code}, cb);
}

bicicletaSchema.statics.updateByCode = function(code, cb) {
    return this.updateOne({code:code}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

// const Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }


// Bicicleta.AllBicis = [];

// Bicicleta.add = function (bici) {
//     Bicicleta.AllBicis.push(bici);
// }

// Bicicleta.findById = function(id) {

//     const bici = Bicicleta.AllBicis.find(x => x.id === Number(id));
//     if ( bici ) {
//         return bici;
//     } else {
//         throw new Error(`No existe una bicicleta con el id ${id}`);
//     }
// }

// Bicicleta.remove = function(id) {
 
//     bicis = [...Bicicleta.AllBicis];
//     bicis = bicis.filter(x=> x.id !== Number(id));
//     Bicicleta.AllBicis = bicis;
// }

// Bicicleta.update = function(bici) {
    
//     bicis = [...Bicicleta.AllBicis];
//     bicis = bicis.filter(x=>{ 
//       if( x.id === Number(bici.id)) { 
//           x = {...bici};
//           return x;
//       } 
//       return x;
//     });
// }

// // const bici1 = new Bicicleta(1, 'rojo', 'urbana', [6.230833, -75.590553]);
// // const bici2 = new Bicicleta(2, 'blanca', 'urbana', [6.2386 ,-75.5793]);

// // Bicicleta.add(bici1);
// // Bicicleta.add(bici2);

// module.exports = Bicicleta;