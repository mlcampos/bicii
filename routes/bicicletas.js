const express = require('express');
const router = express.Router();

const { bicicleta_list, 
        bicicleta_create_post, 
        bicicleta_create_get,
        bicicleta_remove,
        bicicletas_update_get,
        bicicletas_update_post } = require('../controllers/bicicleta');

router.get('/', bicicleta_list );
router.get('/create', bicicleta_create_get)
router.post('/create', bicicleta_create_post)
router.post('/:id/delete', bicicleta_remove)
router.get('/:id/update', bicicletas_update_get)
router.post('/:id/update', bicicletas_update_post)

module.exports = router;