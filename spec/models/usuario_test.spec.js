const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const Usuario = require('../../models/usuario');
const Reserva = require('../../models/reserva');

describe('Testing Usuarios', () => {
    
    beforeAll(async ()=>{
        await mongoose.disconnect();
    });

    beforeEach((done) => {

        const mongodb = 'mongodb://localhost/testdb'
        mongoose.connect(mongodb, { 
            useNewUrlParser: true, 
            useUnifiedTopology: true, 
            useCreateIndex: true
        });
        let db = mongoose.connection;
        db.on('error', console.error.bind(console, 'conecction error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done) => {

        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success) {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });


    describe('Un usuario reserva una bicicleta', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'camilo'});
            usuario.save();
            const bicicleta = new Bicicleta({"code":1, "color":"verde", "modelo":"nueva", "lat":6.230833, "lng":75.590553});
            bicicleta.save();

            const hoy = new Date();
            const manana = new Date();
            manana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva) {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });



});