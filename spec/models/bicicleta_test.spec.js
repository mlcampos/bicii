const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

describe('Bicicletas', () => {
    
    beforeAll(async ()=>{
        await mongoose.disconnect();
    });

    beforeEach((done) => {

        const mongodb = 'mongodb://localhost/testdb'
        mongoose.connect(mongodb, { 
            useNewUrlParser: true, 
            useUnifiedTopology: true, 
            useCreateIndex: true
        });
        let db = mongoose.connection;
        db.on('error', console.error.bind(console, 'conecction error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done) => {

        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        })
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            const bici = Bicicleta.createInstance(1,"verde", "urbana", [6.230833, -75.590553]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bicicleta', (done) => {
            const bici = Bicicleta.createInstance(1,"verde", "urbana", [6.230833, -75.590553]);
            Bicicleta.add(bici, function(err, bici) {
                if ( err ) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].color).toBe(bici.color);
                    expect(bicis[0].code).toBe(bici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe de volver la bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){

                expect(bicis.length).toBe(0);

                const bici = Bicicleta.createInstance(1,"verde", "urbana", [6.230833, -75.590553]);
                Bicicleta.add(bici, (err) => {
                    if (err) console.log(err);
                    const bici2 = Bicicleta.createInstance(2,"cafe", "urbana", [6.230840, -75.590555]);

                    Bicicleta.add(bici, (err) => {
                        if (err) console.log(err);
                        Bicicleta.findByCode(bici.code, function(error, biciFinded) {
                            if (error) console.log(error);
                            expect(biciFinded.code).toEqual(bici.code);
                            expect(biciFinded.color).toBe(bici.color);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('remueve una sola bicicleta por su code', (done) => {
            const bici = Bicicleta.createInstance(1,"verde", "urbana", [6.230833, -75.590553]);
            Bicicleta.add(bici, (err) => {
                if (err) console.log(err);
                Bicicleta.removeByCode(bici.code, function(error) {
                    if (error) console.log(error);
                    Bicicleta.allBicis(function(elErr, bicis) {
                        if (elErr) console.log(elErr);
                        expect(bicis.length).toBe(0);
                    });
                    done();
                });
            })
        });
    });

});

// describe('Bicicletas', () => {

//     beforeEach(() => {
//         Bicicleta.AllBicis = [];
//     });

//     describe('bicicleta.allBicis', () => {
//         it('comienza vacia', () => {
//             expect(Bicicleta.AllBicis.length).toBe(0);
//         });
//     });
    
//     describe('bicicleta.add', () => {
        
//         it('bicicleta.allBicis deberia tener una bicicleta', () => {
    
//             expect(Bicicleta.AllBicis.length).toBe(0);
    
//             const bici = new Bicicleta(1, 'verde', 'nueva', [6.230833, -75.590553]);
//             Bicicleta.add(bici);
//             expect(Bicicleta.AllBicis.length).toBe(1);
//             expect(Bicicleta.AllBicis[0]).toBe(bici);
//         });
//     });
    
//     describe('Bicicleta.findById', () => {
//         it('deberia de volver una bicicleta por su id', () => {
    
//             expect(Bicicleta.AllBicis.length).toBe(0);
    
//             const bici = new Bicicleta(1, 'verde', 'nueva', [6.230833, -75.590553]);
//             Bicicleta.add(bici);
//             const biciFinded = Bicicleta.findById(bici.id);
//             expect(biciFinded).toEqual(bici);
//             expect(bici.color).toBe(biciFinded.color);
//             expect(bici.modelo).toBe(biciFinded.modelo);
    
//         });
//     });
    
//     describe('Bicicleta.remove', () => {
//         it('elimina una bicicleta', () => {
//             expect(Bicicleta.AllBicis.length).toBe(0);
    
//             const bici = new Bicicleta(1, 'verde', 'nueva', [6.230833, -75.590553]);
//             const bici2 = new Bicicleta(2, 'cafe', 'nueva', [6.230833, -75.590553]);
//             Bicicleta.add(bici);
//             Bicicleta.add(bici2);
//             Bicicleta.remove(1);
//             expect(Bicicleta.AllBicis.length).toBe(1);
//             expect(Bicicleta.AllBicis[0].modelo).toBe(bici2.modelo);
//         });
//     });
// });

