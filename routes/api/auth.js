const express = require('express');
const router = express.Router();
const authController = require('../../controllers/api/authControllerApi'); 

router.get('/authenticate', authController.authenticate);
router.post('/forgoPassword', authController.forgotPassword);

module.exports = router;