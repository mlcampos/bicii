var map = L.map('main_map', {
    center: [6.230833, -75.590553],
    zoom: 13
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result) {
    console.log(result);
    result.bicicletas.forEach(bici => {
        L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
    });
  }
});


// function onMapClick(e) {
//     const latlng = e.latlng;
//     L.marker([latlng.lat, latlng.lng]).addTo(map);
// }
// map.on('click', onMapClick);

