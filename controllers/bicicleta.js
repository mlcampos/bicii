const Bicicleta = require('../models/bicicleta');

bicicleta_list = (req, res) => {
    Bicicleta.allBicis(function(err, bicis){
        res.render('bicicletas/index', {bicis})
    })
}

bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
}

bicicleta_create_post = (req, res) => {
    const { id, color, modelo, lat, lng } = req.body;
    let bici = new Bicicleta(id, color, modelo, [lat, lng]);
    Bicicleta.add(bici);
    res.redirect( 200, '/bicicletas');
}

bicicleta_remove = (req, res) => {
    Bicicleta.remove(req.body.id);
    res.redirect('/bicicletas');
}

bicicletas_update_get = (req, res) => {
    const bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', { bici });
}

bicicletas_update_post = (req, res) => {
    const bici = Bicicleta.findById(req.params.id);
    const { id, color, modelo, lat, lng } = req.body;
    bici.id = id;
    bici.color = color;
    bici.modelo = modelo;
    bici.ubicacion = [lat, lng];
    res.redirect(200, '/bicicletas');
}

module.exports = {
    bicicleta_list,
    bicicleta_create_get,
    bicicleta_create_post,
    bicicleta_remove,
    bicicletas_update_get,
    bicicletas_update_post
}